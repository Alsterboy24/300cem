# README #

This README would normally document whatever steps are necessary to get your application up and running.

### PSreview Repository ###

* This repository is for the source code i.e Java class files, 
* drawable files
* layout xml files
* Android Manifest
* Gradle files etc.

### PSReview Explained ###

* This Application was built for the PlayStation gamer, more specifically those who game on the PS4
* The User is able to: 
* create an account
* login with that account
* view upcoming games for the PS4 and current trending games
* visit the PlayStation Store via a simple WebView
* access latest news about PlayStation through WebView & view their articles
* view their current location through Google Maps and see Game Store markers
* view information about the trending games, for example game director, programmer etc.
* read basic plot summary of each game.
* watch game trailer
* user can write reviews about these games
* these reviews are saved to a database like the user account details
* all reviews can be viewed (data persistence)
* able to edit account details (username, name, email, password)